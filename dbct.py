import datetime
import sys
import configparser
import psycopg2


def open_db(SCHEMA,TABLE,NAME,TOTAL,DB_NAME,DB_USR,DB_PASS,USER):
 conn = psycopg2.connect("dbname='"+DB_NAME+"' user='"+DB_USR+"' password='"+DB_PASS+"' host='devtritium01d'")
 cur = conn.cursor()
 output_data(SCHEMA,TABLE,NAME,TOTAL,conn,cur,USER)

def output_data(SCHEMA,TABLE,NAME,TOTAL,conn,cur,USER):
 values ="'" + NAME + "'" + "," + "'" + str(TOTAL) + "'" + "," + "'" + str(datetime.datetime.now()) + "'" + "," + "'" + "" + "'" + "," + "'" + USER + "'"
 query = "INSERT INTO "+SCHEMA+"."+TABLE+" VALUES ("+values+")"
 cur.execute(query,(values))
 conn.commit()
 cur.close()
 conn.close()

def open_db_err(SCHEMA,TABLE,NAME,TOTAL,DB_NAME,DB_USR,DB_PASS,out,USER):
 conn = psycopg2.connect("dbname='"+DB_NAME+"' user='"+DB_USR+"' password='"+DB_PASS+"' host='devtritium01d'")
 cur = conn.cursor()
 output_data_err(SCHEMA,TABLE,NAME,TOTAL,conn,cur,out,USER)

def output_data_err(SCHEMA,TABLE,NAME,TOTAL,conn,cur,out,USER):
 values ="'" + NAME + "'" + "," + "'" + str(TOTAL) + "'" + "," + "'" + str(datetime.datetime.now()) + "'" + "," + "'" + out + "'" + "," + "'" + USER + "'"
 query = "INSERT INTO "+SCHEMA+"."+TABLE+" VALUES ("+values+")"
 cur.execute(query,(values))
 conn.commit()
 cur.close()
 conn.close()  

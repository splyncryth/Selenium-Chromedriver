import argparse
import configparser

def userinput():
 parser = argparse.ArgumentParser()
 parser.add_argument('-C', action="store",type=str, dest="config_path", required=True)
 parser.add_argument('-V', action="store_true", dest="view")
 parser.add_argument('-P', action="store", type=str, dest ="prof")
 parser.add_argument('-I', action="store", type=int, dest ="int_count", default=1)
 return vars(parser.parse_args())

def what(what):
 args = userinput()
 config=configparser.ConfigParser()
 config.read(args['config_path'])
 if what == "DB_USR":
  return config.get("configuration","dbusr")
 elif what == "DB_PASS":
  return config.get("configuration","dbpwd")
 elif what == "USER":
  return config.get("configuration","acc_usr")
 elif what == "PASSWD":
  return config.get("configuration","acc_pwd")
 elif what == "DOMAIN":
  return config.get("configuration","domain")
 elif what == "DB_NAME":
  return config.get("configuration","db_name")
 elif what == "SCHEMA":
  return config.get("configuration","schema")
 elif what == "TABLE":
  return config.get("configuration","table")
 elif what == "PATH":
  return config.get("configuration","path")
 elif what == "FILE":
  return config.get("configuration","file")
 elif what == "SUBDOMAIN":
  return config.get("configuration","subdomain")
 elif what == "DB_HOST":
  return config.get("configuration","host")
 elif what == "DRIVER":
  return config.get("configuration","driver")
 else:
  return None


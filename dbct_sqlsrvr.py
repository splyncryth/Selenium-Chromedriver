import datetime
import sys
import configparser
import pymssql

def open_db(DB_HOST,TABLE,NAME,TOTAL,DB_NAME,DB_USR,DB_PASS,USER,OFFSET):
 conn = pymssql.connect(DB_HOST,DB_USR,DB_PASS,DB_NAME)
 cur = conn.cursor()
 output_data(TABLE,NAME,TOTAL,conn,cur,USER,OFFSET)

def output_data(TABLE,NAME,TOTAL,conn,cur,USER,OFFSET):
 values =""+NAME+","+TOTAL+","+str(datetime.datetime.now())+","+USER+""
 query = "INSERT INTO dbo."+TABLE+"(process, time, timestamp, user_name, offset) VALUES('"+NAME+"','"+TOTAL+"','"+str(datetime.datetime.now())+"','"+USER+"','"+OFFSET+"')"
 cur.execute(query)
 conn.commit()
 cur.close()
 conn.close()

def open_db_err(DB_HOST,TABLE,NAME,TOTAL,DB_NAME,DB_USR,DB_PASS,out,USER):
 conn = pymssql.connect(DB_HOST,DB_USR,DB_PASS,DB_NAME)
 cur = conn.cursor()
 output_data_err(TABLE,NAME,TOTAL,conn,cur,out,USER)

def output_data_err(TABLE,NAME,TOTAL,conn,cur,out,USER):
 values =""+NAME+","+TOTAL+","+str(datetime.datetime.now())+","+USER+""
 query = "INSERT INTO dbo."+TABLE+"(process, time, timestamp, error, user_name) VALUES('"+NAME+"','"+TOTAL+"','"+str(datetime.datetime.now())+"','"+out+"','"+USER+"')"
 cur.execute(query)
 conn.commit()
 cur.close()
 conn.close() 



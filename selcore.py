from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.webdriver.common.keys import Keys
import time
import datetime 
import sys
import erhand
import confs

driver= None
hover= ""
element_to_hover_over= ""
submit= ""
select = ""
name = ""

def create_driver(driver_path,TIME):
 chrome_options = Options()
 chrome_options.add_argument("window-size=1200,600")
 chrome_options.add_argument("--user-data-dir=/tmp/Chromedriver."+str(datetime.datetime.now()))
 chrome_options.add_argument("--no-sandbox")
 global driver
 driver = webdriver.Chrome(driver_path,chrome_options=chrome_options)
 driver.implicitly_wait(TIME)

def screenshot(path):
 driver.save_screenshot(path)

def hover_by_ID(TIME, element):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.ID, element)))
 hover = ActionChains(driver).move_to_element(one)
 hover.perform()

def hover_by_xpath_txt(TIME,element):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.XPATH,"//*[text()[contains(.,'"+element+"')]]")))
 hover = ActionChains(driver).move_to_element(one)
 hover.perform()

def select_dropdown(TIME,element,viz_text):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.ID, element)))
 #select = Select(driver.find_element_by_id(element))
 select = Select(one)
 select.select_by_visible_text(viz_text)

def click_by_name(TIME,element):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.ID, element)))
 one.click()

def click_by_id(TIME,element):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.ID, element)))
 one.click()

def send_key(TIME,element,key):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.NAME, element)))
 #name = driver.find_element_by_name(one)
 one.send_keys(key)

def send_key_id(TIME,element,key):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.ID, element)))
 #name = driver.find_element_by_id(one)
 one.send_keys(key)

def tab_click(element):
 name = driver.find_element_by_id(element)
 ActionChains(driver).send_keys(Keys.LEFT_CONTROL).click(name).perform()

def move_to_active(tab):
 driver.switch_to_window(driver.window_handles[tab])

def close_tab():
 driver.close()

def accept_alert():
 alert = driver.switch_to.alert
 alert.accept()

def click_save(TIME, element):
#This works only when loading a new page with new HTML body elements
 old_element = driver.find_element_by_id(element)
 submit = driver.find_element_by_id(element).click()
 WebDriverWait(driver, TIME).until(staleness_of(old_element))

def find_partial_link(element):
 link = driver.find_element_by_partial_link_text(element)

def find_by_xpath_txt(TIME,element):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.XPATH,"//*[text()[contains(.,'"+element+"')]]")))

def click_partial_link(TIME,element):
 old_element = driver.find_element_by_partial_link_text(element)
 submit = driver.find_element_by_partial_link_text(element).click()
 WebDriverWait(driver, TIME).until(staleness_of(old_element))

def if_tag_exists(element):
 one=driver.find_element_by_tag_name(element)

def focus_iframe(TIME,element):
 iframe= WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.XPATH, "//*[@id='"+element+"']")))
 driver.switch_to.frame(iframe)

def click_by_xpath_txt(TIME,element):
 one = WebDriverWait(driver, TIME).until(EC.presence_of_element_located((By.XPATH,"//*[text()[contains(.,'"+element+"')]]")))
 one.click()

def print_window_handle():
 print(driver.window_handles)

def current_window_handle():
 print(driver.current_window_handle)

